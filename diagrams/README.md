# MP

A VSCode, instal.lar els dos següents plugins:

* PlantUML (jebbs.plantuml)
* AsciiDoc (asciidoctor.asciidoctor-vscode)

Per a executar AsciiDoc, cal configurar utilitzar Kroki.
Buscar a VSCode el setting "asciidoc.use_kroki" i activar-lo.

Els diagramas PlantUML es poden exportar des del el menú paleta de VSCode:
* PlantUML: Export current diagram
Això crearà un arxiu a la carpeta out.

També pots utilitzar el [servidor online](https://www.plantuml.com/plantuml/uml/SyfFKj2rKt3CoKnELR1Io4ZDoSa70000) de la web de PlantUML.


Els diagrames AsciiDoc es poden exportar també desde la paleta de VSCode:
* AsciiDoc: Save HTML Document
Això crearà un document HTML que es pot copiar.

# Kroki

Opcionalment, es pot utilitzar un servidor propi.

[Instal.lació manual](https://docs.kroki.io/kroki/setup/manual-install/)

Des de la línia de comanda:
`$ java -jar kroki-server-v0.XX.0.jar`

Això arrenca un servidor al port 8000.

Cal configurar a VSCode:
* Asciidoc > Preview: Attributes: cal editar settings.json i afegir aquestes la entrada:

```json
"asciidoc.preview.attributes": {
  "kroki-server-url": "http://localhost:8000"
},
```
