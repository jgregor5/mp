[plantuml]
....

skinparam BoxPadding 15

box "127.0.0.1" #eee
actor Client as u
participant "Peer1" as p1
endbox
box "127.0.0.2" #eee
participant "Peer2" as p2
endbox

u -> p1: ping("127.0.0.2")
activate p1
p1 -> p2: [1]
activate p2
p1 -> u: Future f
deactivate p1
p2 -> p1: [2]
deactivate p2
activate p1
p1 -> p1: f.complete()
deactivate p1

....