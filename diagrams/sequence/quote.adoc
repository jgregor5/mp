[plantuml]
....

!pragma teoz true

actor user
participant QuoteView
participant QuotePresenter
participant QuoteModel

QuotePresenter->QuoteView: setListener(this)
& QuotePresenter->QuoteModel: setListener(this)

user->QuoteView: click Button
QuoteView->QuotePresenter: onButtonClicked()
QuotePresenter->QuoteModel: requestMessage()
QuoteModel->QuotePresenter: onMessageReady("...")
QuotePresenter->QuoteView: setMessage("...")
QuoteView->user: label.setText("...")

....
